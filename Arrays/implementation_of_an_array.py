from typing import Any


class Array:
    """
    Implement an Array
    """

    def __init__(self):
        # constructor
        self.length: int = 0
        self.data: dict = {}

    def __str__(self):
        # represent class obj as string
        return str(self.__dict__)

    def get(self, index: int) -> Any:
        """Get the value at the 
        specified index
        """
        return self.data[index]

    def push(self, item: Any) -> None:
        """
        Insert an element
        """
        self.data[self.length] = item
        self.length += 1

    def pop(self) -> Any:
        """
        Remove the last element
        """
        lastitem: Any = self.data[self.length - 1]
        del self.data[self.length - 1]
        self.length -= 1
        return lastitem

    def delete(self, index: int) -> Any:
        """
        Remove an element at an index
        """
        deleteditem: Any = self.data[index]
        for i in range(index, self.length - 1):
            self.data[i] = self.data[i + 1]

        del self.data[self.length - 1]
        self.length -= 1
        return deleteditem


if __name__ == "__main__":
    arr = Array()
    arr.push(3)
    arr.push(4)
    arr.push("hello")
    arr.push(5)
    arr.push(22)
    arr.push("hey")
    arr.push("welcome")
    arr.delete(3)
    print(arr)
